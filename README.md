Ansible Role for ACME-DNS
===

This ansible role is deploying acme-dns (https://github.com/joohoi/acme-dns).
Tested on Debian 10 and Ubuntu 20.04

## How To

You'll need to set the following Variables in your Ansible:
```yaml
# FQDN of your ACME-DNS Server
acmedns__domain: "acme-dns.infra.run"
# NS Entry of your ACME-DNS Server (mostly identical to your FQDN)
acmedns__nameserver_name: "acme-dns.infra.run"
# Admin Mail where "@" is substituted with "."
acmedns__nameserver_admin: "ops.infra.run"
```

More optional Vars can be found at ```defaults/main.yml```
The most important ones of them are:
```yaml
#Disable registrations at your acme-dns server
acmedns__disable_registration: "false"
# Define what acme-dns should do, to get a Certificate for its API. Possible values, when using this role: "letsencrypt", "letsencryptstaging", "none"
acmedns__tls_method: "letsencrypt"

# ACME-DNS logging level: "error", "warning", "info" or "debug"
acmedns__logging_level: "warning"
```